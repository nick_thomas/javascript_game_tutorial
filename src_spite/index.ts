const canvas: HTMLCanvasElement = document.getElementById('canvas1')! as HTMLCanvasElement;
const ctx: CanvasRenderingContext2D = canvas.getContext('2d') as CanvasRenderingContext2D;

const CANVAS_WIDTH = canvas.width = 600;
const CANVAS_HEIGHT = canvas.height = 600;

const playerImage = new Image();
playerImage.src = './src/assets/shadow_dog.png';
const spriteWidth = 575; // calculate total width of sprite / column
const spriteHeight = 523; // calculate total height of sprite/number of rows


// Type definitions
enum animationStatesValues {
    IDLE = "idle",
    JUMP = "jump",
    FALL = "fall",
    RUN = "run",
    DIZZY = "dizzy",
    SIT = "sit",
    ROLL = "roll",
    BITE = "bite",
    KO = "ko",
    GETHIT = "getHit"
}
type Key = animationStatesValues;
type animationStatesType = {
    name: Key,
    frames: number,
};
type frameType = {
    loc: Array<{ x: number, y: number }>
}
type spriteAnimationType = Record<Key, frameType>;

// Player control logic
let playerControls = animationStatesValues.IDLE;
const createLabels = document.getElementById("controls")!;
createLabels.appendChild((() => {
    const label = document.createElement("label");
    label.setAttribute("for", "animations");
    label.innerHTML = "Choose Animation";
    return label;
})());
createLabels.appendChild((() => {
    const select = document.createElement("select");
    select.setAttribute("id", "animations");
    select.setAttribute("name", "animations");
    select.innerHTML = "Choose Animation";
    for (const value in animationStatesValues) {
        const option = document.createElement("option");
        option.setAttribute("value", value);
        let [head, ...tail] = [...value];
        for (const item of tail) {
            head += (item.toLowerCase());
        }
        option.innerHTML += head;
        select.appendChild(option);
    };
    select.addEventListener('change', (event: Event) => {
        const key = (event.target as HTMLInputElement).value;
        playerControls = (<any>animationStatesValues)[key];
    })
    return select;
})());

// gameFrame checks if frame is too fast
let gameFrame = 0;
// Frame rate limiter
const staggerFrame = 5;
const spriteAnimations: spriteAnimationType = {} as spriteAnimationType;
const animationStates: animationStatesType[] = [
    {
        name: animationStatesValues.IDLE,
        frames: 7,
    },
    {
        name: animationStatesValues.JUMP,
        frames: 7,
    },
    {
        name: animationStatesValues.FALL,
        frames: 9,
    },
    {
        name: animationStatesValues.RUN,
        frames: 9,
    },
    {
        name: animationStatesValues.DIZZY,
        frames: 11,
    },
    {
        name: animationStatesValues.SIT,
        frames: 5,
    },
    {
        name: animationStatesValues.ROLL,
        frames: 7,
    },
    {
        name: animationStatesValues.BITE,
        frames: 7,
    },
    {
        name: animationStatesValues.KO,
        frames: 12,
    },
    {
        name: animationStatesValues.GETHIT,
        frames: 4,
    }
];
animationStates.forEach((state, index) => {
    let frames: frameType = {
        loc: [],
    }
    for (let j = 0; j < state.frames; j += 1) {
        let positionX = j * spriteWidth;
        let positionY = index * spriteHeight;
        frames.loc.push({ x: positionX, y: positionY });
    }
    spriteAnimations[state.name] = frames;
})


function animate() {
    //    ctx.fillRect(50, 50, 100, 100); // Draw on the canvas/ animate
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT); //Clears the canvas
    // Calculating the position
    let position = Math.floor(gameFrame / staggerFrame) % spriteAnimations[playerControls].loc.length;
    // Following variables are used to iterate through sprites frame wise x & y
    const frameX = spriteWidth * position;
    const frameY = spriteAnimations[playerControls].loc[position].y;
    // Draws image on the canvas from src,stretches and crops items like background position
    ctx.drawImage(playerImage, frameX, frameY, spriteWidth, spriteHeight, 0, 0, CANVAS_WIDTH, CANVAS_WIDTH);
    gameFrame += 1;
    requestAnimationFrame(animate);
}
animate();

