import {
  JumpingLeft,
  JumpingRight,
  RunningLeft,
  RunningRight,
  SitingLeft,
  SitingRight,
  StandingLeft,
  StandingRight,
  STATES,
} from "./state";

export default class Player {
  public gameWidth: number;
  public gameHeight: number;
  public states: [
    StandingLeft,
    StandingRight,
    SitingLeft,
    SitingRight,
    RunningLeft,
    RunningRight,
    JumpingLeft,
    JumpingRight
  ];
  public currentState:
    | StandingLeft
    | StandingRight
    | SitingLeft
    | SitingRight
    | RunningLeft
    | RunningRight
    | JumpingLeft
    | JumpingRight;
  public image: HTMLCanvasElement;
  public width: number;
  public height: number;
  public x: number;
  public y: number;
  public frameX: number;
  public frameY: number;
  public speed: number;
  public maxSpeed: number;
  public vy: number;
  public weight: number;

  constructor(gameWidth: number, gameHeight: number) {
    this.gameWidth = gameWidth;
    this.gameHeight = gameHeight;
    this.states = [
      new StandingLeft(this),
      new StandingRight(this),
      new SitingLeft(this),
      new SitingRight(this),
      new RunningLeft(this),
      new RunningRight(this),
      new JumpingLeft(this),
      new JumpingRight(this),
    ];
    this.currentState = this.states[0];
    this.image = document.getElementById("dogImage") as HTMLCanvasElement;
    this.width = 200;
    this.height = 181.83;
    this.x = this.gameWidth / 2;
    -this.width / 2;
    this.y = this.gameHeight - this.height;
    this.frameX = 0;
    this.frameY = 0;
    this.speed = 0;
    this.maxSpeed = 10;
    this.vy = 0;
    this.weight = 0.5;
  }
  public draw(context: CanvasRenderingContext2D) {
    context.drawImage(
      this.image,
      this.width * this.frameX,
      this.height * this.frameY,
      this.width,
      this.height,
      this.x,
      this.y,
      this.width,
      this.height
    );
  }

  update(input: string) {
    this.currentState.handleInput(input);
    this.x += this.speed;
    if (this.x <= 0) this.x = 0;
    else if (this.x >= this.gameWidth - this.width) {
      this.x = this.gameWidth - this.width;
    }
    this.y += this.vy;
    if (!this.onGround()) {
      this.vy += this.weight;
    } else {
      this.vy = 0;
    }
    if (this.y > this.gameHeight - this.height) {
      this.y = this.gameHeight - this.height;
    }
  }

  public setState(state: STATES) {
    this.currentState = this.states[state];
    this.currentState.enter();
  }

  public onGround(): boolean {
    return this.y >= this.gameHeight - this.height;
  }
}
