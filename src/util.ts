import Player from "./player";
import { STATES } from "./state";

interface InputI {
  lastKey: string;
}

export function drawStatusText(
  context: CanvasRenderingContext2D,
  input: InputI,
  player: Player
) {
  context.font = "28px Helvetica";
  context.fillText("Last input : " + input.lastKey, 10, 50);
  context.fillText(
    "Active state: " + STATES[player.currentState.state],
    20,
    90
  );
}
