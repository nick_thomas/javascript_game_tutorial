import InputHandler from "./input";
import Player from "./player";
import { drawStatusText } from "./util";

window.addEventListener("load", function () {
  const loading = document.getElementById("loading") as HTMLElement;
  loading.style.display = "none";
  const canvas = document.getElementById("canvas1") as HTMLCanvasElement;
  const ctx = canvas.getContext("2d")!;
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  const player = new Player(canvas.width, canvas.height);
  const input = new InputHandler();

  function animate() {
    // clear canvas with each re-render
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    player.update(input.lastKey);
    player.draw(ctx);
    drawStatusText(ctx, input, player);
    requestAnimationFrame(animate);
  }
  animate();
});
