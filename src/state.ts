import Player from "./player";

export enum STATES {
  STANDING_LEFT,
  STANDING_RIGHT,
  SITTING_LEFT,
  SITTING_RIGHT,
  RUNNING_LEFT,
  RUNNING_RIGHT,
  JUMPING_LEFT,
  JUMPING_RIGHT,
}

class State {
  public state: STATES;
  constructor(state: STATES) {
    this.state = state;
  }
}

export class StandingLeft extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.STANDING_LEFT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 1;
    this.player.speed = 0;
  }
  handleInput(input: string): void {
    if (input === "PRESS right") {
      // set state to StandingRight
      this.player.setState(STATES.RUNNING_RIGHT);
    } else if (input === "PRESS left") {
      this.player.setState(STATES.RUNNING_LEFT);
    } else if (input === "PRESS down") {
      this.player.setState(STATES.SITTING_LEFT);
    } else if (input === "PRESS up") {
      this.player.setState(STATES.JUMPING_RIGHT);
    }
  }
}

export class StandingRight extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.STANDING_RIGHT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 0;
    this.player.speed = 0;
  }
  handleInput(input: string): void {
    if (input === "PRESS left") {
      // set state to StandingLeft

      this.player.setState(STATES.RUNNING_LEFT);
    } else if (input === "PRESS right") {
      this.player.setState(STATES.RUNNING_RIGHT);
    } else if (input === "PRESS down") {
      this.player.setState(STATES.SITTING_RIGHT);
    } else if (input === "PRESS up") {
      this.player.setState(STATES.JUMPING_LEFT);
    }
  }
}

export class SitingLeft extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.STANDING_RIGHT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 9;
    this.player.speed = 0;
  }
  handleInput(input: string): void {
    if (input === "PRESS right") {
      this.player.setState(STATES.SITTING_RIGHT);
    } else if (input === "PRESS up") {
      this.player.setState(STATES.STANDING_LEFT);
    }
  }
}
export class SitingRight extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.STANDING_RIGHT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 8;
    this.player.speed = 0;
  }
  handleInput(input: string): void {
    if (input === "PRESS right") {
      this.player.setState(STATES.SITTING_LEFT);
    } else if (input === "PRESS up") {
      this.player.setState(STATES.STANDING_RIGHT);
    }
  }
}
export class RunningLeft extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.RUNNING_LEFT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 7;
    this.player.speed = -this.player.maxSpeed;
  }
  handleInput(input: string): void {
    if (input === "PRESS right") {
      this.player.setState(STATES.RUNNING_RIGHT);
    } else if (input === "RELEASE left") {
      this.player.setState(STATES.STANDING_LEFT);
    } else if (input === "PRESS down") {
      this.player.setState(STATES.SITTING_LEFT);
    }
  }
}
export class RunningRight extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.RUNNING_RIGHT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 6;
    this.player.speed = this.player.maxSpeed;
  }
  handleInput(input: string): void {
    if (input === "PRESS left") {
      this.player.setState(STATES.RUNNING_LEFT);
    } else if (input === "RELEASE right") {
      this.player.setState(STATES.STANDING_RIGHT);
    } else if (input === "PRESS down") {
      this.player.setState(STATES.SITTING_RIGHT);
    }
  }
}
export class JumpingLeft extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.JUMPING_LEFT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 3;
    if (this.player.onGround()) {
      this.player.vy -= 10;
    }
    this.player.speed = -this.player.maxSpeed * 0.5;
  }
  handleInput(input: string): void {
    if (input === "PRESS right") this.player.setState(STATES.JUMPING_RIGHT);
    else if (this.player.onGround()) {
      this.player.setState(STATES.STANDING_LEFT);
    }
  }
}
export class JumpingRight extends State {
  public player: Player;
  constructor(player: Player) {
    super(STATES.JUMPING_RIGHT);
    this.player = player;
  }
  enter(): void {
    this.player.frameY = 2;
    if (this.player.onGround()) {
      this.player.vy = -10;
    }
    this.player.speed = this.player.maxSpeed * 0.5;
  }
  handleInput(input: string): void {
    if (input === "PRESS left") this.player.setState(STATES.JUMPING_LEFT);
    else if (this.player.onGround()) {
      this.player.setState(STATES.STANDING_RIGHT);
    }
  }
}
